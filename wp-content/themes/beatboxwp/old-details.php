<div class="bb-details-inner-element bb-details-header">
          <div class="container-fluid">
            <div class="row">
              <div class="col-xs-12 col-md-6">
                <div class="bb-details-header-right">
                  <div class="bb-details-art">
                    <div class="bb-details-art-inner">
                      
                      <?php
                        // Post thumbnail.
                        twentyfifteen_post_thumbnail();
                      ?>

                    </div>
                  </div>
                  <div class="bb-details-nav">
                    <div class="bb-details-nav-prev">
                      <?php next_post_link('<i class="fa fa-chevron-up"></i>'); ?>
                      
                    </div>
                    <div class="bb-details-nav-next">
                      <?php previous_post_link('<i class="fa fa-chevron-down"></i>'); ?>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-md-6">
                <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
              </div>
              
            </div>
          </div>
        </div>

        <div class="bb-details-inner-element bb-details-attributes">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <?php
                $posttags = get_the_tags();
                if ($posttags) {
                  echo "<ul>";
                  foreach($posttags as $tag) {
                    echo "<li>";
                    echo $tag->name . ' ';
                    echo "</li>";
                  }
                  echo "</ul>";
                }
                ?>
              </div>
            </div>
          </div>
        </div>

        <div class="bb-details-inner-element bb-details-description">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-10">
                <?php the_content(); ?>
              </div>
            </div>
          </div>
        </div>

        <div class="bb-details-inner-element bb-details-cta">
          <div class="container-fluid">
            <div class="row">
              <div class="bb-details-cta-col col-sm-4">
                <div class="bb-details-cta-button">
                  <a href="<?php echo get_post_meta($post->ID, 'cta', true); ?>" target="_blank"><?php echo get_post_meta($post->ID, 'cta-label', true); ?></a>
                  
                </div>
              </div>
              <div class="bb-details-cta-col col-sm-2">
                <div class="bb-details-cta-price">
                  $<?php echo get_post_meta($post->ID, 'price', true); ?>
                </div>
              </div>
              <div class="bb-details-cta-col col-sm-2">
                <div class="bb-details-cta-edit">
                  <?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>
                </div>
              </div>
            </div>
          </div>
        </div>