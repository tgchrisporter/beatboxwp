<?php
/**
 * The template used for displaying sound content
 *
 * @package WordPress
 * @subpackage beatboxwp
 * @since Beatbox WP 1.0
 */
?>

<?php
	$cellClasses = array(
		'mix',
		'bb-grid-cell',
		'col-lg-2',
		'col-md-3',
		'col-sm-4',
		'col-xs-6',
	);
?>

<li id="post-<?php the_ID(); ?>" <?php post_class( $cellClasses ); ?> data-my-order="<?php echo get_post_field('menu_order', $post->ID); ?>">
	<div class="bb-grid-img">
		<?php
			// Post thumbnail.
			twentyfifteen_post_thumbnail('full');
		?>
	</div>
</li><!-- #post-## -->
