<?php

/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage beatboxwp
 * @since beatboxwp 1.0
 */
?><!DOCTYPE html>
<html <?php
language_attributes(); ?> class="no-js">
<head>
	<title><?php wp_title(' | ', true, 'right'); ?></title>
	<meta charset="<?php
bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php
bloginfo('pingback_url'); ?>">
	<link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<!--[if lt IE 9]>
	<script src="<?php
echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script src="https://use.typekit.net/mwb8apb.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>

<body <?php body_class( 'skin-dark' ); ?>>


	<div id="page" class="bb-body-wrapper">
	  <header class="bb-header">
	    <div class="bb-header-menu-wrapper">
	    	<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
	    </div>

	    <div class="bb-header-wrapper">
	      <div class="container-fluid">
	        <div class="row">
	          <div class="col-xs-4">
	            <div class="bb-header-element bb-header-filter">
	              Filter
	            </div>
	          </div>

	          <div class="col-xs-4">
	            <div class="bb-header-element bb-header-logo">
	              <a href="<?php $url = home_url( $path = '/', $scheme = relative ); echo $url; ?>">The Beatbox Club</a>
	            </div>
	          </div>

	          <div class="col-xs-4">
	            <div class="bb-header-element bb-header-menu">
	              <span class="bb-header-menu-trigger">
	              	<span class="icon-close">
	              	  <span class="dot"></span>
	              	  <span class="dot"></span>
	              	  <span class="dot"></span>
	              	</span>
	              </span>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </header>

	  <div class="bb-header-filter-wrapper">
	    <div class="container-fluid">
	      <div class="row">
	        <div class="col-md-12">
	          <ul>
	          	<?php wp_reset_query(); if (is_post_type_archive('plug')) { ?>

		            <li class="filter" data-filter="all">All</li>

		            <li class="filter" data-filter=".category-people">People</li>

		            <li class="filter" data-filter=".category-tutorials">Tutorials</li>

		            <li class="filter" data-filter=".category-downloads">Downloads</li>

	            <?php } else { ?>

		            <li class="filter" data-filter="all">All</li>

		            <li class="filter" data-filter=".category-instrumental-albums">Instrumental Albums</li>

		            <li class="filter" data-filter=".category-soundkits">Sound Kits</li>

	            <?php } ?>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </div>

	  <div id="content" class="bb-content-wrapper">

