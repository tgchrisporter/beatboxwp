$(function() {
  
  // Open in New Window
  function newTab() {
    $("a[href^='http://']").attr("target", "_blank");
    $("a[href^='https://']").attr("target", "_blank");
  }
  newTab();

  // Filter Engine
  $('#Container').mixItUp({
    animation: {
      animateChangeLayout: true,
      duration: 400,
      effects: 'fade translateZ(-360px) stagger(100ms)',
    	easing: 'cubic-bezier(0.6839, 0.1, 0.32, 1.175)',
    }
  });
  
  $('#Container').on('mixEnd', function(e, state){
    console.log(state.totalShow+' elements match the current filter');
  });
  
  // Filter Trigger
  $('.bb-header-filter').click(function(){
    $('.bb-header-filter-wrapper').toggleClass('bb-header-filter-wrapper-on');
    $('.bb-grid').toggleClass('filter-on');
  });
  
  // Menu Trigger
  $('.bb-header-menu').click(function(){
    $('.bb-header').toggleClass('bb-header-menu-on');
    $('body').toggleClass('overflow-hidden');
  });
  

  // Open Details
  var soundTarget = $('.post-type-archive-sound .bb-grid-element, .post-type-archive-sound .bb-grid-img');

  $.ajaxSetup({cache:false});     
  soundTarget.on('click',function(){

    var post_link = $(this).children('a').attr("href");
    var loadingicon = $('#bb_loading');
    var ajax = $(".bb-details-inner-ajax");
    var ajaxCon = $(".bb-details-inner-ajax-container");

    ajaxCon.removeClass('fade-in').addClass('fade-out');

    $('.bb-content-wrapper').addClass('bb-details-on');
    $('body,.bb-body-wrapper').addClass('overflow-hidden');

    setTimeout(function(){
      loadingicon.addClass('loading');
    }, 500);
    setTimeout(function(){
      loadingicon.removeClass('loading');
      newTab();
      ajax.load(post_link);
    }, 1000);
    setTimeout(function(){
      ajaxCon.removeClass('fade-out').addClass('fade-in');
    }, 1500);
    

    return false;
  });
  
  // Close Details
  $('.bb-details-cover, .bb-details-close').click(function(){
    var ajax = $(".bb-details-inner-ajax");
    ajax.empty();
    $('.bb-content-wrapper').removeClass('bb-details-on');
    $('body,.bb-body-wrapper').removeClass('overflow-hidden');
  });


  // Nav AJAX
  
  $(document).on( 'click', '.bb-details-nav a', function( event ) {
    event.preventDefault();

    var post_linkb = $(this).attr("href");

    var ajax = $(".bb-details-inner-ajax");
    var ajaxCon = $(".bb-details-inner-ajax-container");

    var loadingicon = $('#bb_loading');

    ajaxCon.removeClass('fade-in').addClass('fade-out');
    
    setTimeout(function(){
      loadingicon.addClass('loading');
    }, 500);
    setTimeout(function(){
      loadingicon.removeClass('loading');
      newTab();
      ajax.load(post_linkb);
    }, 1000);
    setTimeout(function(){
      ajaxCon.removeClass('fade-out').addClass('fade-in');
    }, 1500);

  });
  
  // Hide/Show Box
  $('.bb-update-toggle').click(function(){
    if($('.bb-update').hasClass('bb-update-hide')){
      $('.bb-update').removeClass('bb-update-hide');
      $('.bb-update-toggle').text('Hide Me');
    }
    else {
      $('.bb-update').addClass('bb-update-hide');
      $('.bb-update-toggle').text('Read Me');
    }
  });
});