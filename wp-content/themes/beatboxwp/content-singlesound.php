<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="bb-details-col bb-details-col-left">
    <div class="bb-details-art">
      <div class="bb-details-art-inner">
        
        <?php
          // Post thumbnail.
          twentyfifteen_post_thumbnail();
        ?>

      </div>
    </div>
    <div class="bb-details-header">
      <?php the_title( '<h3 class="bb-details-header-heading">', '</h3>' ); ?>
    </div>
  </div>

  <div class="bb-details-content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <div class="bb-details-inner-element bb-details-description">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-4">
          <div class="bb-details-inner-element bb-details-attributes">
            <?php
              $posttags = get_the_tags();
              if ($posttags) {
                echo "<ul>";
                foreach($posttags as $tag) {
                  echo "<li>";
                  echo $tag->name . ' ';
                  echo "</li>";
                }
                echo "</ul>";
              }
            ?>
          </div>
        </div>
        <div class="col-xs-12 col-md-4">
          <div class="bb-details-inner-element bb-details-cta-price">
            <div class="bb-details-ie-pad">
              $<?php echo get_post_meta($post->ID, 'price', true); ?>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-4">
          <div class="bb-details-inner-element bb-details-cta-button">
            <a href="<?php echo get_post_meta($post->ID, 'cta', true); ?>" target="_blank"><?php echo get_post_meta($post->ID, 'cta-label', true); ?></a>
          </div>
        </div>
      </div>
    </div>
  </div>

</article>