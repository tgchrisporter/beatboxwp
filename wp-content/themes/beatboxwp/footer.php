<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
		
		</div><!-- .site-content -->
	
	</div><!-- .bb-body-wrapper -->

</div><!-- .site -->

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="https://dl.dropboxusercontent.com/u/80054631/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.4.1/snap.svg-min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/global.js"></script>

<?php wp_footer(); ?>

</body>
</html>
