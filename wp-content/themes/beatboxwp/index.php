<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage beatboxwp
 * @since Beatbox WP 1.0
 */

get_header(); ?>
	

	<section class="bb-content-element bb-grid">
	  <div class="container-fluid">

	  	<header class="bb-page-header row">
	  		<div class="col-xs-12">
	  			<h1 class="bb-page-header-ele bb-page-header-heading">Kits &amp; Beats</h1>
	  			<p class="bb-page-header-ele bb-page-header-paragraph">Step it up with these sound kits and instrumental albums, carefully curated by The Beatbox Club.</p>
	  		</div>
	  	</header>

	  	<ul class="row" id="Container">

				<?php if ( have_posts() ) : ?>

					<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'content', 'sound' );

					// End the loop.
					endwhile;

					// Previous/next page navigation.
					the_posts_pagination( array(
						'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
						'next_text'          => __( 'Next page', 'twentyfifteen' ),
						'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
					) );

				// If no content, include the "No posts found" template.
				else :
					get_template_part( 'content', 'none' );

				endif;
				?>

			</ul>
		</div><!-- .site-main -->
	</section><!-- .content-area -->

	<section class="bb-content-element bb-details">
	  <div class="bb-details-cover"></div>
	  <div class="bb-details-inner">
	  	<div id="bb_loading" class="bb-loading-icon">
	  		<div class="loading-logo"><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div><div class="box"></div></div>
	  	</div>

	    <div class="bb-details-inner-element bb-details-close">
	      &times;
	    </div>

	    <div class="bb-details-inner-ajax-container">
	    	<div class="bb-details-inner-ajax"></div>
	    </div>
	  </div>
	</section>

<?php get_footer(); ?>
