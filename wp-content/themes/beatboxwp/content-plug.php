<?php
/**
 * The template used for displaying plug content
 *
 * @package WordPress
 * @subpackage beatboxwp
 * @since Beatbox WP 1.0
 */
?>

<?php
  $cellClasses = array(
    'mix',
    'bb-grid-cell',
    'col-xs-12',
  );
?>

<li id="post-<?php the_ID(); ?>" <?php post_class( $cellClasses ); ?> data-my-order="<?php echo get_post_field('menu_order', $post->ID); ?>">
  <div class="row">
    <div class="col-sm-2 col-md-2 col-lg-1">
      <div class="bb-grid-img">
        <a href="<?php the_field('plug_link'); ?>">
          <?php the_post_thumbnail(); ?>
        </a>
      </div>
    </div>
    <div class="col-sm-10 col-md-10 col-lg-11">
      <div class="bb-plug-right">
        <div class="row">
          <div class="col-sm-10">
            <div class="bb-plug-title">
              <a href="<?php the_field('plug_link'); ?>"><?php the_title(); ?></a>
            </div>
          </div>
          <div class="col-sm-2">  
            <div class="bb-likes">
              <?php if( function_exists('zilla_likes') ) zilla_likes(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</li><!-- #post-## -->
