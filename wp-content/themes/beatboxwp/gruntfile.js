module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    copy: {
      dev: {
        src: 'readme.txt',
        dest: 'README.md'
      }
    },

    sass: {
      dist: {
        options: {
          style: 'expanded'
        },
        files: {
          'style.css': 'scss/style.scss',
        }
      }
    },

    watch: {
      css: {
        files: '**/*.scss',
        // tasks: ['sass', 'concat', 'cssmin'],
        tasks: ['sass'],
        options: {
          livereload: true,
        },
      },
      options: {
        livereload: true,
      },
    },
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['copy', 'sass', 'watch']);
  grunt.registerTask('dev', ['copy', 'sass', 'watch']);
};