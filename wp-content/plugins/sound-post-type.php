<?php
/*Plugin Name: Create Sound Post Type
Description: This plugin registers the 'Sound' post type.
Version: 1.0
License: GPLv2
*/

	function Sounds() {
		// set up labels
		$labels = array(
			'name' => 'Sounds',
			'singular_name' => 'Sound',
			'add_new' => 'Add New Sound',
			'add_new_item' => 'Add New Sound',
			'edit_item' => 'Edit Sound',
			'new_item' => 'New Sound',
			'all_items' => 'All Sounds',
			'view_item' => 'View Sound',
			'search_items' => 'Search Sounds',
			'not_found' => 'No Sounds Found',
			'not_found_in_trash' => 'No Sounds found in Trash',
			'parent_item_colon' => '',
			'menu_name' => 'Sounds',
		);
		//register post type
		register_post_type( 'sound', array(
			'labels' => $labels,
			'has_archive' => true,
			'public' => true,
			'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
			'taxonomies' => array( 'post_tag', 'category' ),
			'exclude_from_search' => false,
			'capability_type' => 'post',
			'rewrite' => array( 'slug' => 'sounds' ),
			'menu_icon' => 'dashicons-playlist-audio',
		)
		);
	}
	add_action( 'init', 'Sounds' );
?>