<?php
/*Plugin Name: Create Plug Post Type
Description: This plugin registers the 'Plug' post type.
Version: 1.0
License: GPLv2
*/

  function Plugs() {
    // set up labels
    $labels = array(
      'name' => 'Plugs',
      'singular_name' => 'Plug',
      'add_new' => 'Add New Plug',
      'add_new_item' => 'Add New Plug',
      'edit_item' => 'Edit Plug',
      'new_item' => 'New Plug',
      'all_items' => 'All Plugs',
      'view_item' => 'View Plug',
      'search_items' => 'Search Plugs',
      'not_found' => 'No Plugs Found',
      'not_found_in_trash' => 'No Plugs found in Trash',
      'parent_item_colon' => '',
      'menu_name' => 'Plugs',
    );
    //register post type
    register_post_type( 'plug', array(
      'labels' => $labels,
      'has_archive' => true,
      'public' => true,
      'supports' => array( 'title', 'editor', 'excerpt', 'custom-fields', 'thumbnail','page-attributes' ),
      'taxonomies' => array( 'post_tag', 'category' ),
      'exclude_from_search' => false,
      'capability_type' => 'post',
      'rewrite' => array( 'slug' => 'plugs' ),
      'menu_icon' => 'dashicons-admin-plugins',
    )
    );
  }
  add_action( 'init', 'Plugs' );
?>